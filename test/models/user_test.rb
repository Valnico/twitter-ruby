require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should not save user without name" do
    user = User.new
    user.img_url = "test"
    assert_not user.save, 'Saved user without name'
  end

  test "should not save user if name length < 3" do
    user = User.new
    user.name = "te"
    assert_not user.save, 'Saved user with name length < 3'
  end

  test "should not save user if name length > 20" do
    user = User.new
    user.name = "testOfUserNameVeryLong"
    assert_not user.save, 'Saved user with name length > 20'
  end

  test "should save user if name length = 3" do
    user = User.new
    user.name = "tes"
    assert user.save, 'Not saved user with name length = 3'
  end

  test "should save user if name length = 20" do
    user = User.new
    user.name = "testOfUserNameVeryLo"
    assert user.save, 'Not saved user with name length = 20'
  end

  test "should not save user if name length contains spaces" do
    user = User.new
    user.name = "test Of User"
    assert_not user.save, 'Saved user with name that contains spaces'
  end

  test "should not save user with same name" do
    user = User.new
    user.name = "test"
    user.save
    user2 = User.new
    user2.name = "test"
    assert_not user2.save, 'Saved user with same name'
  end
end
