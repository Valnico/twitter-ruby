require 'test_helper'

class TweetTest < ActiveSupport::TestCase
  test 'should not save tweet without tweet' do
    tweet = Tweet.new
    tweet.user_id = 1
    assert_not tweet.save, 'Saved tweet without tweet'
  end

  test 'should not save tweet without user_id' do
    tweet = Tweet.new
    tweet.tweet = "Test de tweet"
    assert_not tweet.save, 'Saved tweet without user_id'
  end

  test 'should save tweet' do
    tweet = Tweet.new
    tweet.user_id = 1
    tweet.tweet = "Test de tweet"
    assert tweet.save, 'Not saved tweet'
  end
end
