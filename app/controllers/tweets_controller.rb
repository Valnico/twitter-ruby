class TweetsController < ApplicationController
  before_action :set_tweet, only: [:show, :edit, :update, :destroy]

  # GET /tweets
  # GET /tweets.json
  def index
    @user = User.find(params[:user_id])
    @tweets = @user.tweets
  end

  # GET /tweets/1
  # GET /tweets/1.json
  def show
  end

  # GET /tweets/new
  def new
    @tweet = Tweet.new
    @user = User.find(params[:user_id])
  end

  # GET /tweets/1/edit
  def edit
  end

  # POST /tweets
  # POST /tweets.json
  def create
    @tweet = Tweet.new(tweet_params)
    @user = User.find(params[:user_id])
    if @user.save
      @tweet = @user.tweets.create(tweet_params)
      redirect_to user_path(@user)
    else
      flash[:error] = 'Erreur lors de la création du tweet'
      redirect_to user_path(@user)
    end
  end

  # PATCH/PU@tweets
  # PATCH/PU@tweetson
  def update
    respond_to do |format|
      if @tweet.update(tweet_params)
        format.html { redirect_to @tweet, notice: 'Tweet was successfully updated.' }
        format.json { render :show, status: :ok, location: @tweet }
      else
        format.html { render :edit }
        format.json { render json: @tweet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tweets/1@tweets
  # DELETE /tweets/1.json@tweets
  def destroy
    @user = User.find(params[:user_id])
    @tweet = @user.tweets.find(params[:id])
    @tweet.destroy
    respond_to do |format|
      format.html { redirect_to user_path(@user), notice: 'Le tweet a bien été supprimer !' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tweet
      @tweet = Tweet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tweet_params
      params.require(:tweet).permit(:tweet)
    end
end
