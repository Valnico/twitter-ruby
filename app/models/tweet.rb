class Tweet < ActiveRecord::Base
    belongs_to :user
    validates :tweet, presence: true
    validates :user_id, presence: true
end
