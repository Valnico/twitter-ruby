class User < ActiveRecord::Base
    has_many :tweets, dependent: :destroy
    validates :name, presence: true,
                     uniqueness: true,
                     length: {
                        minimum: 3,
                        maximum: 20
                     },
                     format: {
                        with: /\A[a-zA-Z0-9]+\Z/
                     }

    def image
        if img_url.blank?
            ""
        else
            img_url
        end
    end
end


