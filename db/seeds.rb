# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user1 = User.create(
  name: 'user1',
  img_url: 'https://i.ytimg.com/vi/T60B8V8IjVs/maxresdefault.jpg',
)

user2 = User.create(
  name: 'user2',
  img_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/Michel_Cremades.jpg/220px-Michel_Cremades.jpg',
)

tweet1 = Tweet.create(
    user_id: user1.id,
    tweet: "Les tweets c'est rigolo" 
)

tweet2 = Tweet.create(
    user_id: user1.id,
    tweet: 'Ce tweeter a changé ma vie'
)

tweet3 = Tweet.create(
    user_id: user2.id,
    tweet: "Avec comme Pierre, j'ai perdu 250€ en 1 semaine"
)