#Read Me

## 1. Installation

First clone the git repository.
Second access it and run "bundle install --path vendor/bundle".
Then, run "npm install".
Finally, run "rake db:setup"
Now you should have everything you need to run Twitter! (the fake one)

## 2. Launch the server

Run "rails s" to launch the server in dev mode.
You can access it using your web browser with the following address: "localhost:3000".
